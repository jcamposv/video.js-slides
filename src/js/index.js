import videojs from 'video.js';
import document from 'global/document';
import SiemaExtend from './library/siema-extend.js';
import './helpers.js';
// Default options for the plugin.
const defaults = {};

// Cross-compatibility for Video.js 5 and 6.
const registerPlugin = videojs.registerPlugin || videojs.plugin;
// const dom = videojs.dom || videojs;

/**
 * Function to invoke when the player is ready.
 *
 * @function onPlayerReady
 * @param    {Player} player
 *           A Video.js player.
 * @param    {Object} [options={}]
 *           An object of options left to the plugin author to define.
 */
class VideoSlides {
  /**
  * VideoSlides Constructor.
  *
  * @classdesc A class that represents a slides.
  * @constructs VideoSlides
  * @param {Object} player current video player instance
  * @param {Array} options Array Object Items has slide image and time to show the slide
  */
  constructor(player, options) {
    // Get the player element object
    this.el = player.el();
    // This is the video slides items will show in the right corner of the video
    this.videoSlidesItems = options.slides;
    // Will get the author name and image
    this.author = options.author;
    // List of authors and links
    this.resources = options.resources;
    // This will be the logo for video footer
    this.logo = options.logo;
    // This will be links for footer video
    this.links = options.links;
    /* Will check if slides come with defaultOpen
    true and will show on full mode the slide into the video */
    this.defaultOpen = options.slides.defaultOpen;
    /* This is a flag to check if the next slide will
    show on full mode or thubmnail mode */
    this.defaultOpenActive = false;
    /* This will check if the current time is different
    to before time when timeupdate call*/
    this.oldTime = 0;
    // This will be the item per page for carousel
    this.itemPerPage = 6;
    this.wrapVideoDivs();
    this.wrapVideo();
    this.appendSlider();
    this.appendModes();
    this.appendFullMode();
    this.buildFooter();
    this.moveProgressControl();
    this.bindEvents();
  }
  /**
  * AppendSlider function to append Slide list into VideoJS.
  *
  * @return {void} doesn't return enething
  * @function appendSlider
  *
  */
  appendSlider() {
    // Get control bar element
    const controlBar = this.el.getElementsByClassName('vjs-control-bar')[0];

    // Ceate a slides list
    this.videoSlidelist = document.createElement('ul');
    // Create slides thubmnail container
    this.videoSlidesContainer = document.createElement('div');
    // Add list of slides into slides container div parent
    this.videoSlidesContainer.appendChild(this.videoSlidelist);
    // Add slide list className
    this.videoSlidelist.className = 'video-slides';
    // Add video slides container class and insert it on videojs container div
    this.videoSlidesContainer.className = 'video-slides-container';
    controlBar.parentNode.insertBefore(this.videoSlidesContainer, controlBar);
    this.appendSliderItem();
    /* Get video-slides instance after was created */
    this.videoSlideList = this.el.getElementsByClassName('video-slides')[0];
  }
  /**
  * AppendMode function to append over modes to the  slides list into VideoJS.
  *
  * @return {void} doesn't return enething
  * @function appendModes
  *
  */
  appendModes() {
    // Create links and span element for over div state to update the video mode show
    const fullMode = document.createElement('a');
    const halfMode = document.createElement('a');
    const fullHalfModeChild = document.createElement('span');
    const fullModeCopy = document.createTextNode('Full Mode');
    const halfModeCopy = document.createTextNode('Half Mode');

    // Add custom class into new elements
    fullMode.className = 'video_full_mode full-screen-icon';
    halfMode.className = 'video_half_mode half-screen-icon';
    // Create the Overlay Div for the video slides list in the right corner of the video
    this.overDiv = document.createElement('div');
    this.overDiv.className = 'video-mode';
    // Append Custom elements into new divs
    fullMode.appendChild(fullHalfModeChild);
    halfMode.appendChild(fullModeCopy);
    fullHalfModeChild.appendChild(halfModeCopy);
    this.overDiv.appendChild(halfMode);
    this.overDiv.appendChild(fullMode);
    this.videoSlidesContainer.appendChild(this.overDiv);
    // Clone overDiv element and append into video wrap
    const overDivVideo = this.overDiv.cloneNode(true);

    overDivVideo.className += ' video-mode-overlay';
    this.videoContainer.appendChild(overDivVideo);
  }
  /**
  * appendFullMode function to add full screen button on half mode.
  *
  * @return {void} doesn't return enething
  * @function appendFullMode
  *
  */
  appendFullMode() {
    // Create element for full mode icon into video and slides container on half mode show
    const fullModeVideo = document.createElement('a');
    const fullModeSlide = document.createElement('a');
    const fullModeVideoSpan = document.createElement('span');
    const fullModeSlideSpan = document.createElement('span');
    const fullModeCopy = document.createTextNode('Full Mode');
    const fullModeCopyVideo = document.createTextNode('Full Mode');
    const videoContainer = this.videoContainer;

    // Add custom clases
    fullModeVideo.className = 'full-screen-icon thumb';
    fullModeSlide.className = 'full-screen-icon thumb';
    // Append new elements into video and slides container
    fullModeVideo.appendChild(fullModeVideoSpan);
    fullModeSlide.appendChild(fullModeSlideSpan);
    fullModeVideoSpan.appendChild(fullModeCopyVideo);
    fullModeSlideSpan.appendChild(fullModeCopy);
    videoContainer.appendChild(fullModeVideo);
    this.videoSlidesContainer.appendChild(fullModeSlide);
  }
  /**
  * AppendSliderItem function to append SlideItem  into Slide List.
  *
  * @return {void} doesn't return enething
  * @function appendSliderItem
  *
  */
  appendSliderItem() {
    for (let i = 0; i <= this.videoSlidesItems.length - 1; i++) {
      // Create an image and li tag
      const img = document.createElement('img');
      const li = document.createElement('li');

      // Added src and Class name
      img.src = this.videoSlidesItems[i].url;
      li.className = 'slide_' + this.videoSlidesItems[i].time;
      // Append image into li
      li.appendChild(img);
      // Append li into ul list
      this.videoSlidelist.appendChild(li);
    }
  }
  /**
  * buildResourceList function to build the resources list that come into options
  *
  * @return {void} doesn't return enething
  * @function buildResourceList
  *
  */
  buildResourceList() {
    this.resourceList = document.createElement('ul');

    this.resourceList.className = 'resources-list';
    for (let i = 0; i < this.resources.length; i++) {
      const item = document.createElement('li');
      const link = document.createElement('a');
      const linkDiv = document.createElement('div');
      const name = document.createElement('p');
      const icon = document.createElement('span');
      const text = document.createTextNode(this.resources[i].name);

      link.href = this.resources[i].url;
      link.innerHTML = this.resources[i].cta;
      icon.className = this.resources[i].icon;
      name.appendChild(icon);
      name.appendChild(text);
      item.appendChild(name);
      linkDiv.appendChild(link);
      item.appendChild(linkDiv);
      this.resourceList.appendChild(item);
    }
    this.overlaySlides.appendChild(this.resourceList);
  }
  /**
  * moveProgressControl move the player control outside the wrapper video
  *
  * @return {void} doesn't return enething
  * @function moveProgressControl
  *
  */
  moveProgressControl() {
    const controlBar = this.el.getElementsByClassName('vjs-control-bar')[0];
    const videoFooter = this.el.getElementsByClassName('video-footer')[0];

    controlBar.parentNode.removeChild(controlBar);
    this.el.insertBefore(controlBar, videoFooter);
  }
  /**
  * buildFooter will create the footer video
  *
  * @function buildFooter
  *
  */
  buildFooter() {
    // Create the footer video and append the author, slides, resource, links and logo
    const footer = document.createElement('div');
    const colLeft = document.createElement('div');
    const colRight = document.createElement('div');
    const link1 = document.createElement('div');
    const link2 = document.createElement('div');
    const author = this.authorBuild();

    this.overlaySlidesList = this.videoSlidelist.cloneNode(true);
    this.overlaySlides = document.createElement('div');
    // if author is true will append the author sctruture
    if (author) {
      colLeft.appendChild(author);
    }
    // if slides or resource come will append link1 div with slides or resources link
    if (this.videoSlidelist || this.resources) {
      // Add link 1 div into footer
      colLeft.appendChild(link1);
      link1.className = 'video-link1 video-link';
      // if come slides will append slide link into footer
      if (this.videoSlidelist) {
        link1.appendChild(
          this.linkBuild('Slides', '#', 'slide-link link-list', false, 'fa fa-caret-up')
        );
      }
      // if come resources will append resource link into footer
      if (this.resources) {
        link1.appendChild(
          this.linkBuild('Resources', '#', 'resource-link link-list', false, 'fa fa-caret-up')
        );
      }
    }
    if (this.links) {

      colRight.appendChild(link2);
      link2.className = 'video-link2 video-link';
      for (let i = 0; i < this.links.length; i++) {
        link2.appendChild(
          this.linkBuild(this.links[i].name, this.links[i].url, 'custom-link')
        );
      }
    }

    if (this.logo) {
      colRight.appendChild(
        this.linkBuild(this.logo.url, this.logo.link, 'logo-link', true)
      );
    }
    // Add author name into footer
    footer.className = 'video-footer';
    colLeft.className = 'col col-left';
    colRight.className = 'col col-right';
    footer.appendChild(colLeft);
    footer.appendChild(colRight);
    this.overlaySlidesList.className = 'slides-list';
    this.overlaySlides.className = 'overlay-panel';

    // Build Overlay ToolBar Top
    this.toolBarOverlay();

    this.overlaySlides.appendChild(this.overlaySlidesList);
    this.mediaContent.appendChild(this.overlaySlides);
    this.el.appendChild(footer);
    // Add resource list into overlay
    this.buildResourceList();
    // Total of slides per video
    this.totalSlides = this.overlaySlidesList.getElementsByTagName('li').length;

    // Init Slide Carousel(overlay) after append into the DOM
    this.initSlideCarousel();

  }
  /**
  * initSlideCarousel will init the slide carousel into the overlay
  *
  * @function initSlideCarousel
  *
  */
  initSlideCarousel() {
    this.slideCarousel = new SiemaExtend({
      selector: this.el.getElementsByClassName('slides-list')[0],
      duration: 200,
      easing: 'ease-out',
      perPage: this.itemPerPage,
      startIndex: 0,
      draggable: true,
      threshold: 20,
      loop: false
    });
    this.slideCarousel.customClases();
  }
  /**
  * destroySlideCarousel will destroy the slide carousel
  *
  * @function destroySlideCarousel
  *
  */
  destroySlideCarousel() {
    this.slideCarousel.removeAll();
  }
  /**
  * moveSlideCarousel will move the carousel items
  *
  * @param {number} currentIndex will check the current index and will move the carousel
  * @function moveSlideCarousel
  *
  */
  moveSlideCarousel(currentIndex) {
    if (currentIndex % this.itemPerPage === 6 || currentIndex % this.itemPerPage === 0) {
      this.slideCarousel.next(this.itemPerPage - 1);
    }
  }
  /**
  * toolBarOverlay will build the overlay top tooltip
  *
  * @function toolBarOverlay
  *
  */
  toolBarOverlay() {
    const toolBarLeft = document.createElement('div');
    const toolBarRight = document.createElement('div');
    const closeIcon = document.createElement('a');

    this.toolBar = document.createElement('div');
    this.expandIcon = document.createElement('a');
    this.counterFile = document.createElement('p');
    this.counterFile.className = 'counter';
    this.counterFile.innerHTML = 'Cargando...';
    this.expandIcon.className = 'fa fa-angle-up expand-icon';
    this.toolBar.className = 'tool-bar';
    toolBarLeft.className = 'tool-bar-col';
    closeIcon.className = 'fa fa-times close-icon';
    toolBarRight.className = 'tool-bar-col';
    toolBarLeft.appendChild(this.counterFile);
    this.toolBar.appendChild(toolBarLeft);
    this.toolBar.appendChild(toolBarRight);
    toolBarRight.appendChild(this.expandIcon);
    toolBarRight.appendChild(closeIcon);
    this.overlaySlides.appendChild(this.toolBar);
  }
  /**
  * setFileCounter will set the overlay files item counter
  *
  * @param {Object} files number and label for the counter
  * @function authorBuild
  *
  */
  setFileCounter(files) {
    this.counterFile.innerHTML = files.number + ' ' + files.label;
  }
  /**
  * authorBuild function will build the author estructure.
  *
  * @return {bool} will return false if author data doesn't come or author object
  * @function authorBuild
  *
  */
  authorBuild() {
    if (this.author) {
      const author = document.createElement('div');
      const authorImage = document.createElement('img');
      const authorName = document.createElement('span');

      authorName.innerHTML = this.author.name;
      authorImage.src = this.author.image;
      author.className = 'video-author';
      author.appendChild(authorImage);
      author.appendChild(authorName);
      return author;
    }
    return false;
  }
  /**
  * linkBuild function to build links.
  *
  * @param {string} text will be the copy or image
  * @param {string} href will be the link url
  * @param {string} className will be class name for the link
  * @param {Boolen} image will be true if text come like image url
  * @param {string} span if come true will add span into the copy to add icon fonts
  * @return {Obejct} return the object link
  * @function linkBuild
  *
  */
  linkBuild(text, href, className, image = false, span = false) {
    const link = document.createElement('a');

    if (image) {
      const img = document.createElement('img');

      img.src = text;
      link.appendChild(img);
    } else if (span) {
      const spanElement = document.createElement('span');

      spanElement.className = span;
      link.innerHTML = text;
      link.appendChild(spanElement);
    } else {
      link.innerHTML = text;
    }
    link.href = href;
    link.className = className;
    return link;
  }
  /**
  * WrapVideoDivs will wrap all the original video elements into new div
  *
  * @return {void} doesn't return enething
  * @function WrapVideoDivs
  *
  */
  wrapVideoDivs() {
    // Create a media content div
    this.mediaContent = document.createElement('div');
    this.mediaContent.className = 'media-content';
    this.mediaContent.wrapAll(this.el.childNodes);
  }
  /**
  * WrapVideo will wrap video mode overlay into video tag.
  *
  * @return {void} doesn't return enething
  * @function wrapVideo
  *
  */
  wrapVideo() {
    // Create an new div element and add video container class
    const newDiv = document.createElement('div');

    newDiv.setAttribute('class', 'video-container');
    // Append into video-js cotainer
    this.mediaContent.appendChild(newDiv);
    newDiv.appendChild(this.el.getElementsByClassName('vjs-tech')[0]);
    this.videoContainer = this.el.getElementsByClassName('video-container')[0];
  }
  /**
  * slideShow function to show the current slide according to the time.
  *
  * @param {number} time the current video time
  * @return {void} doesn't return enething
  * @function slideShow
  *
  */
  slideShow(time) {
    const currentTime = Math.floor(time);
    const slidePrefixName = 'slide_';

    /* Run all the slides items and check which show it. It's according to the
    current time and the slide time */
    for (let i = 0; i <= this.videoSlidesItems.length - 1; i++) {
      /* Usually the video.js timeupdate call more than 1 time in
      the current second so this if will check that only 1 time
      will run the code into the if after second is updated to
      different time.*/
      if (this.oldTime !== currentTime) {
        /* Will check if current time is the same that comes on slides object
          and hide the old slide and show the current slide
         */
        if (currentTime === this.videoSlidesItems[i].time) {
          // Check if is the first slide to track the old slide or not
          const firstItem = (i === 0) ? 0 : 1;
          const oldItem = slidePrefixName + this.videoSlidesItems[i - firstItem].time;
          const currentItem = slidePrefixName + this.videoSlidesItems[i].time;
          const currentSlide = this.findChildElements(this.videoSlideList, currentItem);

          // Hide all the slides and show the current slide according to the time
          this.hideAllSlides();

          // Show the current slide
          currentSlide.style.display = 'block';

          // Show the current slide active in the overlay
          this.overlaySlideShow({currentItem, oldItem});

          // Open default show slide
          if (this.videoSlidesItems[i].defaultOpen) {
            this.videoContainer.addClass('video-thumbnail');
            this.el.addClass('full-screen-slide-mode');
            this.defaultOpenActive = true;
          } else if (this.defaultOpenActive) {
            this.videoContainer.removeClass('video-thumbnail');
            this.el.removeClass('full-screen-slide-mode');
            this.defaultOpenActive = false;
          }

          // Move the slide carousel when the total slide per page is the last one
          this.moveSlideCarousel(i + 1);
          this.oldTime = currentTime;
          return false;
        } else if (currentTime < this.videoSlidesItems[i].time) {
          // If current time is less than slides time will hide all the slides
          const selector = slidePrefixName + this.videoSlidesItems[i].time;
          const item = this.findChildElements(this.videoSlideList, selector);

          // Reset old time to current time to avoid issues when issue go back in the time
          this.oldTime = currentTime;
          // Reset default video mode
          if (i === 0) {
            this.videoContainer.removeClass('video-thumbnail');
            this.el.removeClass('full-screen-slide-mode');
            this.el.removeClass('half-screen-mode');
            item.style.display = 'none';
          }
        }

      }
    }
  }
  /**
  * hideAllSlides function to hide all the slides.
  *
  * @return {void} doesn't return enething
  * @function hideAllSlides
  *
  */
  hideAllSlides() {
    const slides = this.videoSlidelist.getElementsByTagName('li');

    for (let i = 0; i < slides.length; i++) {
      slides[i].style.display = 'none';
    }
  }
  /**
  * overlaySlideShow function to show the current slide into the carousel.
  *
  * @param {Object} slides object
  * @return {void} doesn't return enething
  * @function overlaySlideShow
  *
  */
  overlaySlideShow(slides) {

    const overlaySlide = this.el.getElementsByClassName('slides-list')[0];
    const currentItem = overlaySlide.getElementsByClassName(slides.currentItem)[0];
    const beforeItem = overlaySlide.getElementsByClassName(slides.oldItem)[0];

    beforeItem.removeClass('current-slide');
    currentItem.addClass('current-slide');
  }
  /**
  * linkSatus will check the status for show up or down arrows.
  *
  * @param {Object} arrow object
  * @return {void} doesn't return enething
  * @function linkSatus
  *
  */
  linkSatus(arrow) {
    if (this.overlaySlides.hasClass('overlay-panel-open')) {
      arrow.addClass('fa-caret-down').removeClass('fa-caret-up');
    } else {
      arrow.addClass('fa-caret-up').removeClass('fa-caret-down');
    }
  }
  /**
  * bindEvents will register custom events.
  *
  * @param {Object} list object
  * @return {void} doesn't return enething
  * @function bindEvents
  *
  */
  openOverlayList(list) {
    if (this.overlaySlides.hasClass('overlay-panel-open') && list.current.hasClass('show')) {
      this.overlaySlides.removeClass('overlay-panel-open');
    } else {
      this.overlaySlides.addClass('overlay-panel-open');
      list.before.removeClass('show');
      list.current.addClass('show');

    }
    this.linkSatus(list.arrow);
    this.setFileCounter({number: list.totalItem, label: list.name});
  }
  /**
  * resestLinkList will reset the status down for all the slides list and resource list cta
  *
  * @return {void} doesn't return enething
  * @function resestLinkList
  *
  */
  resestLinkList() {
    const listsLink = this.el.getElementsByClassName('link-list');

    for (let i = 0; i < listsLink.length; i++) {
      listsLink[i].getElementsByTagName('span')[0].removeClass('fa-caret-down').addClass('fa-caret-up');
    }
  }
  /**
  * bindEvents will bind all the custom events
  *
  * @return {void} doesn't return enething
  * @function bindEvents
  *
  */
  bindEvents() {
    const that = this;
    const videoFull = this.el.getElementsByClassName('video_full_mode');
    const halfMode = this.el.getElementsByClassName('video_half_mode');
    const thumbIcons = this.el.getElementsByClassName('thumb');
    const expandIcon = this.el.getElementsByClassName('expand-icon')[0];
    const closeIcon = this.el.getElementsByClassName('close-icon')[0];
    /* Get Link list in the footer video and bind the
    click event to show or not the overlay list slides or resources.*/
    const linksList = this.el.getElementsByClassName('link-list');

    // Add custom event on all the video mode buttons
    for (let i = 0; i < videoFull.length; ++i) {
      videoFull[i].addEventListener('click', function(e) {
        e.preventDefault();
        const slidesContainer = that.findParentBySelector(this, '.video-js');
        const currentVideoMode = that.findParentBySelector(this, '.video-mode');

        slidesContainer.className += ' full-screen-slide-mode';
        if (currentVideoMode.hasClass('video-mode-overlay')) {
          slidesContainer.removeClass('full-screen-slide-mode');
          that.videoContainer.removeClass('video-thumbnail');
        } else {
          that.videoContainer.className += ' video-thumbnail';
        }
      });
    }
    for (let i = 0; i < halfMode.length; ++i) {
      halfMode[i].addEventListener('click', function(e) {
        e.preventDefault();
        const slidesContainer = that.findParentBySelector(this, '.video-js');
        const videoMode = that.videoContainer;

        if (videoMode !== null) {
          if (videoMode.hasClass('video-thumbnail')) {
            videoMode.removeClass('video-thumbnail');
          }
        }
        slidesContainer.removeClass('full-screen-slide-mode');
        slidesContainer.className += ' half-screen-mode';
      });
    }
    for (let i = 0; i < thumbIcons.length; ++i) {
      thumbIcons[i].addEventListener('click', function(e) {
        e.preventDefault();
        const videoMode = that.findParentBySelector(this, '.video-container');

        if (videoMode === null) {
          const videoContainer = that.videoContainer;

          videoContainer.addClass('video-thumbnail');
          that.el.addClass('full-screen-slide-mode');
        }
        that.el.removeClass('half-screen-mode');
      });
    }

    for (let i = 0; i < linksList.length; i++) {
      let totalItem = that.totalSlides;
      let current = that.overlaySlidesList;
      let before = that.resourceList;
      let name = 'Slides';
      const arrow = linksList[i].getElementsByTagName('span')[0];

      if (linksList[i].hasClass('resource-link')) {
        totalItem = that.resources.length;
        current = that.resourceList;
        before = that.overlaySlidesList;
        name = 'Files';
      }
      linksList[i].addEventListener('click', function(e) {
        e.preventDefault();
        // Reset the arrow position for  all the link list.
        that.resestLinkList();
        // Open the overlay list or hide according to the current status
        that.openOverlayList({totalItem, arrow, current, before, name});
      });
    }
    // Close the Panel Overlay
    closeIcon.addEventListener('click', function(e) {
      e.preventDefault();
      that.overlaySlides.removeClass('overlay-panel-open');
      that.resestLinkList();
      setTimeout(function() {
        that.resourceList.removeClass('show');
        that.overlaySlidesList.removeClass('show');
      }, 600);
    });
    expandIcon.addEventListener('click', function(e) {
      e.preventDefault();
      that.overlaySlides.toggleClass('overlay-panel-expand');
      if (that.overlaySlides.hasClass('overlay-panel-expand')) {
        that.destroySlideCarousel();
        this.removeClass('fa-angle-up').addClass('fa-angle-down');
      } else {
        that.initSlideCarousel();
        this.removeClass('fa-angle-down').addClass('fa-angle-up');
      }
    });

    const overlaySlideItem = this.overlaySlidesList.getElementsByTagName('li');

    for (let i = 0; i < overlaySlideItem.length; ++i) {
      overlaySlideItem[i].addEventListener('click', function(e) {
        e.preventDefault();
        const currentSlide = that.el.getElementsByClassName('current-slide');

        that.el.addClass('half-screen-mode');
        if (currentSlide.length > 0) {
          currentSlide[0].removeClass('current-slide');
        }
        that.hideAllSlides();
        that.videoSlidelist.getElementsByTagName('li')[i].style.display = 'block';
        this.addClass('current-slide');
      });
    }

  }
  /**
  * collectionHas will check if has collections.
  *
  * @param {node} a the current element node
  * @param {string} b the selector name
  * @return {bool} return true or false
  * @function collectionHas
  *
  */
  collectionHas(a, b) {
    for (let i = 0, len = a.length; i < len; i++) {
      if (a[i] === b) {
        return true;
      }
    }
    return false;
  }
  /**
  * collectionHas will check if has collections.
  *
  * @param {node} elm the current element node
  * @param {string} selector the selector name
  * @return {node} return the parent node
  * @function findParentBySelector
  *
  */
  findParentBySelector(elm, selector) {
    const all = document.querySelectorAll(selector);
    let cur = elm.parentNode;

    while (cur && !this.collectionHas(all, cur)) {
      cur = cur.parentNode;
    }
    return cur;
  }
  /**
  * findChildElements will find childs elements.
  *
  * @param {node} ele the current element node
  * @param {string} selector the child selector name
  * @return {node} return the child node
  * @function findChildElements
  *
  */
  findChildElements(ele, selector) {
    for (let i = 0; i < ele.childNodes.length; i++) {
      if (ele.childNodes[i].className === selector) {
        return ele.childNodes[i];
      }
    }
    return false;
  }
 }
const onPlayerReady = (player, options) => {
  player.addClass('vjs-slides');
  const slides = new VideoSlides(player, options);

  player.on('timeupdate', function() {
    slides.slideShow(this.currentTime());
  });
};

/**
 * A video.js plugin.
 *
 * @function slides
 * @param    {Object} [options={}]
 *           An object of options left to the plugin author to define.
 */
const slides = function(options) {
  this.ready(() => {
    onPlayerReady(this, videojs.mergeOptions(defaults, options));
  });
};

// Register the plugin with video.js.
registerPlugin('slides', slides);

// Include the version number.
slides.VERSION = '__VERSION__';

export default slides;
