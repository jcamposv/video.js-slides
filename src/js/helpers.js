Node.prototype.hasClass = function(className) {
  if (this.classList) {
    return this.classList.contains(className);
  }
  return (this.className.indexOf(className) > -1);
};

Node.prototype.addClass = function(className) {
  if (this.classList) {
    this.classList.add(className);
  } else if (!this.hasClass(className)) {
    const classes = this.className.split(' ');

    classes.push(className);
    this.className = classes.join(' ');
  }
  return this;
};

Node.prototype.removeClass = function(className) {
  if (this.classList) {
    this.classList.remove(className);
  } else {
    const classes = this.className.split(' ');

    classes.splice(classes.indexOf(className), 1);
    this.className = classes.join(' ');
  }
  return this;
};

// Wrap an HTMLElement around another HTMLElement or an array of them.
Node.prototype.wrapAll = function(elms) {
  const el = elms.length ? elms[0] : elms;

  // Cache the current parent and sibling of the first element.
  const parent = el.parentNode;

  // Wrap the first element (is automatically removed from its
  // current parent).
  this.appendChild(el);

  // Wrap all other elements (if applicable). Each element is
  // automatically removed from its current parent and from the elms
  // array.
  while (elms.length) {
    this.appendChild(elms[0]);
  }

  // If the first element had a sibling, insert the wrapper before the
  // sibling to maintain the HTML structure; otherwise, just append it
  // to the parent.
  parent.appendChild(this);
};

Node.prototype.toggleClass = function(className) {
  if (!this || !className) {
    return;
  }

  let classString = this.className;
  const nameIndex = classString.indexOf(className);

  if (nameIndex === -1) {
    classString += ' ' + className;
  } else {
    classString = classString.substr(0, nameIndex) + classString.substr(nameIndex + className.length);
  }
  this.className = classString;
};
