import Siema from 'siema';

class SiemaExtend extends Siema {
  constructor(options) {
    super(options);
  }
  customClases(carouselClass = 'carousel', carouselItemClass = 'carousel-item') {
    this.sliderFrame.className = carouselClass;
    const carouselItem = this.sliderFrame.childNodes;

    for (let i = 0; i < carouselItem.length; i++) {
      carouselItem[i].className = carouselItemClass;
    }
  }
  removeAll() {
    const selector = this.selector;
    const el = selector.getElementsByClassName('carousel')[0];
    const parent = el.parentNode;
    // Get all the carousel items div
    const items = this.selector.getElementsByClassName('carousel-item');
    const counter = items.length;

    // Move all children out of the element
    while (el.firstChild) {
      parent.insertBefore(el.firstChild, el);
    }

    // Remove the empty element
    parent.removeChild(el);

    for (let i = 0; i < counter; i++) {
      // Get all the slides and append into the parent UL
      selector.appendChild(items[i].getElementsByTagName('li')[0]);
      if (i === items.length - 1) {
        for (let x = items.length - 1; x >= 0; x--) {
          // Remove all the carousel items
          items[x].parentNode.removeChild(items[x]);
        }
      }
    }

  }
}

export default SiemaExtend;
